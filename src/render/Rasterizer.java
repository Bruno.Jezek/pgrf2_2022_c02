package render;

import model.Vertex;
import raster.Visibility;
import transforms.Vec2D;

public class Rasterizer {
    private Visibility visibility;
    private FuncShader shader = new FuncShader(){

        @Override
        public int shade(Vertex v) {
            return 0xffff00;
        }
    };

    public Rasterizer(Visibility visibility) {
        this.visibility = visibility;
    }

    public void setShader(FuncShader shader) {
        this.shader = shader;
    }

    public void rasterizeTriangle(Vertex a, Vertex b, Vertex c) {
        //TODO dehomog

        //viewport transform
        //todo x,y otocit y, posunout na 0,0, zvetsit na w, h
        int w = visibility.getImg().getWidth();
        int h = visibility.getImg().getHeight();
        Vec2D aVP = new Vec2D(a.getPosition().getX() + 1, -a.getPosition().getY() + 1);
        aVP = aVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        Vec2D bVP = new Vec2D(b.getPosition().getX() + 1, -b.getPosition().getY() + 1);
        bVP = bVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        Vec2D cVP = new Vec2D(c.getPosition().getX() + 1, -c.getPosition().getY() + 1);
        cVP = cVP.mul(new Vec2D((w-1)/2, (h-1)/2));

        visibility.getImg().getGraphics().drawLine(
                (int) aVP.getX(),(int) aVP.getY(),
                (int) bVP.getX(),(int) bVP.getY()
        );
        //usporadat a, b, c aVP, aVP, bVP, CVP podle aVP.y <= bVP.y <= cVP.y

        //rasterize horni polovinu od aVP.y do bVP.y
        //rasterize dolni polovinu od bVP.y do cVP.y
    }

    public void rasterizePoint(Vertex v){
        //TODO dehomog
        //v.dehomog()
        v = v.mul(1/v.getPosition().getW());

        //viewport transform
        //todo x,y otocit y, posunout na 0,0, zvetsit na w, h
        int w = visibility.getImg().getWidth();
        int h = visibility.getImg().getHeight();
        Vec2D vVP = new Vec2D(v.getPosition().getX() + 1, -v.getPosition().getY() + 1);
        vVP = vVP.mul(new Vec2D((w-1)/2, (h-1)/2));
        visibility.setVisiblePixelWithZtest(
                (int) vVP.getX(), (int) vVP.getY(),
                v.getPosition().getZ(), shader.shade(v));
        //System.out.println(x + "" + y);
    }
}
