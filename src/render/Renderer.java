package render;

import com.sun.xml.internal.bind.v2.TODO;
import model.Part;
import model.Scene;
import model.Solid;
import model.Vertex;
import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Mat4Transl;

public class Renderer {
    //Mat4 model, view, projection;
    private Rasterizer rasterizer;
    private Mat4 transformMatrix = new Mat4Identity();

    public Renderer(Rasterizer rasterizer) {
        this.rasterizer = rasterizer;
    }

    public void setTransformMatrix(Mat4Transl transformMatrix) {
        this.transformMatrix = transformMatrix;
    }

    public void render(Scene scene){
        scene.solids.forEach(solid -> render(solid));
    }

    public void render(Solid solid){
        for(Part part:solid.getPartBuffer()){
            switch (part.getTopology()){
                case POINTS:
                    for(int i = 0; i< part.getCount(); i++){
                        int idx = part.getStart() + i;
                        int index = solid.getIndexBuffer().get(idx);
                        Vertex v = solid.getVertexBuffer().get(index);
                        renderPoint(v);
                    }

                    break;
                case LINES:
                    //TODO
                    break;
                case TRIANGLES:
                    for(int i = 0; i< part.getCount(); i++){
                        int iA = part.getStart() + 3*i;
                        int iB = part.getStart() + 3*i + 1;
                        int iiA = solid.getIndexBuffer().get(iA);
                        Vertex a = solid.getVertexBuffer().get(iiA);
                        Vertex b =solid.getVertexBuffer().get(solid.getIndexBuffer().get(
                                part.getStart() + 3*i +1));
                        Vertex c =solid.getVertexBuffer().get(solid.getIndexBuffer().get(
                                part.getStart() + 3*i + 2));
                        renderTriangle(a,b,c);
                    }
                    break;

            }
        }

    }

    private void renderPoint(Vertex v){

    }

    private void renderLine(Vertex a, Vertex b){

    }

    private void renderTriangle(Vertex a, Vertex b, Vertex c){
        rasterizer.rasterizeTriangle(a,b,c);
        rasterizer.rasterizePoint(a);
        rasterizer.rasterizePoint(b);
        rasterizer.rasterizePoint(c);
        //transformace
        a = a.mul(transformMatrix);
        //fast clip
        //TODO

        //orezani z>=0 tedy w>=zn, tedy w>0
        //TODO sort a.z >= b.z >= c.z

        if (a.getPosition().getZ()<0)
            return;
        if (b.getPosition().getZ()<0){
            double s1 = (0 - a.getPosition().getZ())/(a.getPosition().getZ() - b.getPosition().getZ());
            Vertex ab = b.mul(1-s1).add(a.mul(s1));
            Vertex ac = a; //TODO
            //rasterizer.rasterizeTriangle(a, ab, ac);
            rasterizer.rasterizePoint(a);
            rasterizer.rasterizePoint(ab);
            rasterizer.rasterizePoint(ac);
            return;
        }
        //TODO
    }
}
