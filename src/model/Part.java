package model;

public class Part {
    public enum Topology {POINTS, LINES, LINE_STRIP, LINE_LOOP, TRIANGLES, TRIANGLES_STRIP, TRIANGLE_FAN}
    private int start;
    private int count;
    private Topology topology;

    public Part(int start, int count, Topology topology) {
        this.start = start;
        this.count = count;
        this.topology = topology;
    }

    @Override
    public String toString() {
        return "Part{" +
                "start=" + start +
                ", count=" + count +
                ", topology=" + topology +
                '}';
    }

    public int getStart() {
        return start;
    }

    public int getCount() {
        return count;
    }

    public Topology getTopology() {
        return topology;
    }
}
