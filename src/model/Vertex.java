package model;

import transforms.Mat4;
import transforms.Point3D;

public class Vertex implements Vectorizable<Vertex>{
    private final Point3D position;
    private final int color;
    //todo "one"
    public Vertex() {
        this(new Point3D());
    }
    public Vertex(Point3D position) {
        this(position,0xffffff);
    }
    public Vertex(Point3D position, int color){
        this.position = position;
        this.color = color;
    }

    public Point3D getPosition() {
        return position;
    }


    public Vertex mul(double t){
        //TODO
        return this;
    }

    public Vertex add(Vertex v){
        //TODO
        return this;
    }

    public Vertex mul(Mat4 trans){
        /*Vertex res = new Vertex();
        res.position = position.mul(trans);
        return res;*/
        return new Vertex(position.mul(trans));
        /*position = position.mul(trans);
        return this;*/
    }

    public int getColor(){
        return color;
    }
}

