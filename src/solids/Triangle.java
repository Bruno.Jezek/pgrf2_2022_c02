package solids;

import model.Part;
import model.Solid;
import model.Vertex;
import transforms.Point3D;

import java.util.Arrays;

public class Triangle extends Solid{
    public Triangle(){
        super();
        getVertexBuffer().add(new Vertex(new Point3D(0.9,0,0.5)));
        getVertexBuffer().add(new Vertex(new Point3D(0,0.9,0.5)));
        getVertexBuffer().add(new Vertex(new Point3D(-0.9,-0.9,0.5)));
        getIndexBuffer().addAll(Arrays.asList(0,1,2,1,2,2,3));
        getPartBuffer().add(new Part(0, 1, Part.Topology.TRIANGLES));
        getPartBuffer().add(new Part(3, 2, Part.Topology.LINES));
        getPartBuffer().add(new Part(0, 3, Part.Topology.POINTS));
    }
}
